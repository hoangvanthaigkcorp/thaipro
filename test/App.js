import { DrawerContent } from './menu/DrawerContent';
import React, { useState ,useEffect} from 'react';
import {
  Text, ImageBackground, StatusBar, View, Image, StyleSheet,
  Dimensions, KeyboardAvoidingView, TouchableWithoutFeedback, Alert,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LinearGradient from "react-native-linear-gradient";
import {useNetInfo} from '@react-native-community/netinfo';
import {
  createDrawerNavigator,
} from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DanhMucScreen from './src/base/screen/DanhMucScreen'
import BaoCaoScreen from './src/base/screen/BaoCaoScreen'
import HomeScreen from './src/base/screen/HomeScreen'
import { useDispatch, useSelector, Provider } from 'react-redux';
import store from './src/redux/store';
import axios from 'axios';
import { LoginButton } from 'react-native-fbsdk';
import Orientation from 'react-native-orientation';


var Width = Dimensions.get("window").width;
var Height = Dimensions.get('window').height;
StatusBar.setHidden(false, 'slide');
const { width, height } = Dimensions.get('screen');
//menu
const Drawer = createDrawerNavigator();
const loginAPI = 'https://gkservice.gkcorp.com.vn/index.php/api/user/login/';





function Menu() {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="LoginScreen" component={Login} />
      {/* <Drawer.Screen name="HomeScreen" component={HomeScreen} /> */}
      <Drawer.Screen name="Menu" component={TabScreen} />
    </Drawer.Navigator>
  );
}
function LoginScreen({ route, navigation }) {
  const [seePass, check] = React.useState('co');
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [valueOgCode, setValueOgCode] = useState('vietnamlearning');
  const [valueUsername, setValueUsername] = useState('100005');
  const [valuePassword, setValuePassword] = useState('123@abc');
  const [showPassword, setShowPassword] = useState(false);
  const closeModal = () => {
    setShowModal(false);
  };
  const { og_data } = useSelector(state => state.ogData);
  const dispatch = useDispatch();

  const login = async () => {
    if (valueOgCode && valueUsername && valuePassword) {
      setIsLoading(true);
      const url = loginAPI;
      let dataLogin = {};
      dataLogin['og_code'] = valueOgCode;
      dataLogin['username'] = valueUsername;
      dataLogin['password'] = valuePassword;
      try {
        const res = await axios({ method: 'post', url: url, data: dataLogin });
        let data = res.data;
        if (data.status) {
          console.log(data);
          dataLogin['isLogin'] = true;
          //await dispatch(loginSuccess(dataLogin));
          navigation.navigate("Menu");
        } else {
          Alert.alert('Đăng nhập thất bại', 'Mã công ty, tên đăng nhập hoặc mật khẩu không chính xác');
        }
      } catch (error) {
        console.log(error);
        Alert.alert('Đăng nhập thất bại', 'Mã công ty, tên đăng nhập hoặc mật khẩu không chính xác');
      } finally {
        setIsLoading(false);
      }
    } else {
      Alert.alert('Đăng nhập thất bại', 'Vui lòng điền đầy đủ thông tin');
    }
  };


  React.useEffect(() => {
    if (route.params?.MaCT) {
      console.log(route.params.MaCT)
      setValueOgCode(route.params.MaCT)
      setValuePassword(route.params.Ten)
      setShowPassword(route.params.MK)
    }
    if (route.params?.Ten) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
    if (route.params?.MK) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
  }, [route.params?.MaCT, route.params?.Ten, route.params?.MK]);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : 'position'}
    >
      <ImageBackground
        source={require('./image/manhinh.png')}
        style={{
          width: Width,
          height: Height,
          //flexDirection: 'column',
          //alignItems: 'center',
        }}
      >
        {/* <Image source={require('./image/mantren.png')} style={styles.ImageStyle,{width:393,height:300,borderBottomLeftRadius:120,borderBottomRightRadius:120}} /> */}
        <View style={styles.container, { paddingTop: 350 }}>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon1.png')} style={{ marginLeft: 30, width: 30, height: 30 }} />
            <TextInput
              onChangeText={(text) => setValueOgCode(text)}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Mã công ty"
              underlineColorAndroid="transparent"
              value={valueOgCode}
            />
          </View>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon2.png')} style={{ marginLeft: 30, width: 25, height: 30 }} />
            <TextInput
              onChangeText={(text) => setValueUsername(text)}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Tên đăng nhập "
              underlineColorAndroid="transparent"
              value={valueUsername}
            />
          </View>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon3.png')} style={{ marginLeft: 30, width: 25, height: 30 }} />
            <TextInput
              onChangeText={(text) => setValuePassword(text)}
              secureTextEntry={seePass == 'co' ? true : false}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Mật khẩu"
              underlineColorAndroid="transparent"
              value={valuePassword}
            />
            <TouchableOpacity onPress={() => {
              if (seePass == 'co')
                check('khong')
              else
                check('co')
            }
            }>
              <Image source={require('./image/chematkhau.png')} style={{ width: 25, height: 15 }} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => {
            login()
          }
          }>
            <Image source={require('./image/dangnhap.png')} style={{ marginTop: 50, marginLeft: 128, width: 140, height: 50, borderRadius: 80 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            navigation.navigate('Register')
          }
          }>
            <Text style={{ marginTop: 10, marginLeft: 175, width: 140, height: 50 }}>Đăng ký</Text>
          </TouchableOpacity>
          {/* <LoginButton
              style={{marginTop:10,width:100,height:50}}
              publishPermissions={["email"]}
              onLoginFinished={
                (error, result) => {
                  if (error) {
                    alert("Login failed with error: " + error.message);
                  } else if (result.isCancelled) {
                    alert("Login was cancelled");
                  } else {
                    alert("Login was successful with permissions: " + result.grantedPermissions)
                    console.log(result.grantedPermissions);
                  }
                }
              }
              onLogoutFinished={() => alert("User logged out")} /> */}
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
function Register({ navigation }) {
  const [seePass, check] = React.useState('co');

  const [MaCT, setMaCT] = React.useState('');
  const [Ten, setTen] = React.useState('');
  const [MK, setMK] = React.useState('');
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : 'position'}
    >
      <ImageBackground
        source={require('./image/manhinh.png')}
        style={{
          width: Width,
          height: Height
        }}
      >
        <View style={styles.container, { paddingTop: 350 }}>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon1.png')} style={{ marginLeft: 30, width: 30, height: 30 }} />
            <TextInput
              onChangeText={(text) => setMaCT(text)}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Mã công ty"
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon2.png')} style={{ marginLeft: 30, width: 25, height: 30 }} />
            <TextInput
              onChangeText={(text) => setTen(text)}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Tên đăng nhập "
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.SectionStyle}>
            <Image source={require('./image/icon3.png')} style={{ marginLeft: 30, width: 25, height: 30 }} />
            <TextInput
              onChangeText={(text) => setMK(text)}
              secureTextEntry={seePass == 'co' ? true : false}
              style={{ flex: 1, marginLeft: 50, color: 'white' }}
              placeholder="Mật khẩu"
              underlineColorAndroid="transparent"
            />
            <TouchableOpacity onPress={() => {
              if (seePass == 'co')
                check('khong')
              else
                check('co')
            }
            }>
              <Image source={require('./image/chematkhau.png')} style={{ width: 25, height: 15 }} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{ width: 100, height: 100, justifyContent: 'center', marginLeft: 150 }}
            onPress={() =>
              navigation.navigate('Login', {
                MaCT: MaCT,
                Ten: Ten,
                MK: MK,
              })}
          >
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#402cc3', '#04c2e5']} style={{ paddingHorizontal: 20, borderRadius: 8, justifyContent: 'center', paddingVertical: 10 }}>
              <Text style={{ textAlign: 'justify' }}>
                Đăng ký
            </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}
const LoginStack = createStackNavigator();
function Login() {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
      <LoginStack.Screen name="Register" component={Register} options={{ headerShown: true }} />
    </LoginStack.Navigator>
  );
}
const renderIconTabBar = (props, route) => {
  let icon;
  const widthHeight = route === props.state.routes[props.state.index] ? width * 0.08 : width * 0.1;
  const linkIcon = './image/';
  if (route.name === 'HomeScreen') {
    icon = <Image
      resizeMode="contain"
      style={{ width: widthHeight * 0.90, height: widthHeight * 0.90 }}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'logo1click.png') : require(linkIcon + 'logo1.png')}
    />;
  } else if (route.name === 'DanhMucScreen') {
    icon = <Image
      resizeMode="contain"
      style={{ width: widthHeight * 0.90, height: widthHeight * 0.90 }}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'logo2click.png') : require(linkIcon + 'logo2.png')}
    />;
  } else if (route.name === 'BaoCaoScreen') {
    icon = <Image
      resizeMode="contain"
      style={{ width: widthHeight * 0.90, height: widthHeight * 0.90 }}
      source={route === props.state.routes[props.state.index] ? require(linkIcon + 'logo3click.png') : require(linkIcon + 'logo3.png')}
    />;
  }
  return icon;
};

const renderLabelTabBar = (labelTabBar) => {
  let label;
  const styleLabel = { color: '#fff', marginHorizontal: width * 0.02, fontSize: width * 0.045 };
  if (labelTabBar === 'HomeScreen') {
    label = <Text style={styleLabel}>Trang chủ</Text>;
  } else if (labelTabBar === 'DanhMucScreen') {
    label = <Text style={styleLabel}>Danh mục khóa học</Text>;
  } else if (labelTabBar === 'BaoCaoScreen') {
    label = <Text style={styleLabel}>Báo cáo học tập</Text>;
  }
  return label;
};

const RenderTabBarNavigation = (prop) => {
  const styleActive = {
    borderRadius: width * 0.2,
    backgroundColor: 'green',
    padding: width * 0.02,
    justifyContent: 'space-around',
    alignItems: 'center',
  };
  return (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      paddingVertical: 10,
      backgroundColor: '#fff',
    }}>
      {
        prop.state.routes.map((route, index) => {
          return (
            <TouchableWithoutFeedback
              onPress={() => {
                prop.navigation.navigate(route.name);
              }}
              key={index}
            >
              {
                prop.state.index === index ?
                  <LinearGradient colors={['#4327c2', '#00CDE8']}
                    style={[prop.state.index === index && styleActive, { flexDirection: 'row' }]}
                    start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
                    {renderIconTabBar(prop, route)}
                    {renderLabelTabBar(route.name)}
                  </LinearGradient>
                  :
                  <View
                    style={[prop.state.index === index && styleActive, { flexDirection: 'row' }]}>
                    {renderIconTabBar(prop, route)}
                  </View>
              }
            </TouchableWithoutFeedback>
          );
        })
      }
    </View>
  );
};

const Tab = createBottomTabNavigator();
const TabScreen = () => {
  return (
    <Tab.Navigator tabBar={prop => <RenderTabBarNavigation {...prop} />}>
      <Tab.Screen name="HomeScreen" component={HomeScreen} />
      <Tab.Screen name="DanhMucScreen" component={DanhMucScreen} />
      <Tab.Screen name="BaoCaoScreen" component={BaoCaoScreen} />
    </Tab.Navigator>
  );
};
function App() {
  const [visible, setVisible] = useState(false);
  const isConnect = useNetInfo();

  useEffect(() => {
      Orientation.lockToPortrait();
  }, []);

  useEffect(() => {
          checkConnectState();
  }, [isConnect.isConnected]);

  const checkConnectState = () => {
      if (!isConnect.isConnected) {
          setVisible(true);
      } else {
          setVisible(false);
      }
  };

  const _onClose = () => {
      setVisible(false);
  };
  return (
    <Provider store={store}>
      <NavigationContainer>
        {/* <ScreenApp /> */}
        <Menu />
      </NavigationContainer>
    </Provider>
  );
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },

  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00D0BF',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5,
    marginTop: 30,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 0,
  },
  floatingButtonStyle: {
    position: 'absolute',
    bottom: 120,
    right: 0,
    //backgroundColor:'black'
  },
  scrollView: {
  },
});
export default App;
