import React from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
const shadow = {
    shadowColor: '#000',
    shadowOffset: { width: 10, height: 10 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
};
const { width, height } = Dimensions.get('screen');

const _titleCourse = (courseName) => {
    return (
        <Text style={{ color: '#000', fontWeight: '700', fontSize: width * 0.035 }}>{courseName}</Text>
    );
};

const _timeCourse = (status, estimatedTime) => {
    return (
        <View style={{ flexDirection: 'row', marginTop: width * 0.01 }}>
            <View style={{ flex: 4 }}>
                <Text style={{ color: '#989898', fontSize: width * 0.03 }}>{_switchStatus(status)}</Text>
            </View>
            <View style={{ flex: 3 }}>
                <Text style={{ color: '#989898', fontSize: width * 0.03 }}>{`Thời lượng: ${estimatedTime || 0}'`}</Text>
            </View>
        </View>
    );
};

const _accessCourse = (lastAccessedDateTime) => {
    return (
        <View style={{ flexDirection: 'row', marginTop: width * 0.01 }}>
            <View style={{ flex: 4 }}>
                <Text style={{ color: '#989898', fontSize: width * 0.03 }}>Truy cập lần cuối:</Text>
            </View>
            <View style={{ flex: 3 }}>
                <Text style={{ color: '#989898', fontSize: width * 0.03 }}>{lastAccessedDateTime}</Text>
            </View>
        </View>
    );
};

const _buttonCourse = (status) => {
    return (
        <View style={{ width: '100%', alignItems: 'center', marginTop: width * 0.015 }}>
            <LinearGradient colors={['#4327c2', '#00CDE8']}
                style={{
                    borderRadius: width * 0.04,
                    paddingVertical: height * 0.005,
                    paddingHorizontal: width * 0.05,
                }}
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>Mở</Text>
                </View>
            </LinearGradient>
        </View>
    );
};

const _switchStatus = (status) => {
    switch (status) {
        case 'in_process':
            return 'Đang học';
        case 'completed' :
            return 'Đã hoàn thành';
        default:
            return 'Chưa học'
    }
};

const ItemCourse = (props) => {
    return (
        <TouchableOpacity style={[!props.showCategory && !props.showProgramme && shadow, {
            backgroundColor: '#fff',
            marginHorizontal: width * 0.06,
            borderRadius: width * 0.03,
            marginVertical: height * 0.01,
            flexDirection: 'row',
        }]}
            onPress={props.navigation}
        >
            <Image
                source={props.item.CourseImg ? {uri: props.item.CourseImg} : require('../../Image/image/imageCourse.png')}
                // resizeMode={'contain'}
                style={{
                    width: width * 0.24,
                    height: '100%',
                    borderTopLeftRadius: width * 0.03,
                    borderBottomLeftRadius: width * 0.03,
                }}
            />

            <View style={{ flex: 3, padding: width * 0.01 }}>
                {_titleCourse(props.item.CourseName)}
                {_timeCourse(props.item.status, props.item.EstimatedTime)}
                {_accessCourse(props.item.LastAccessedDateTime)}
                {_buttonCourse(props.item.status)}
            </View>
        </TouchableOpacity>
    );
};

export default ItemCourse;
