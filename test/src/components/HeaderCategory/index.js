import React, {useState} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

const HeaderCategory = (props) => {

    return (
        <View style={{width: width, height: width / 1080 * 334}}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
            <ImageBackground style={{
                flex: 1,
                justifyContent: 'flex-end'
            }}
                             source={require('../../Image/image/HeaderCategory.png')}
                             resizeMode={'stretch'}
            >
                <View style={{
                    width: WidthPercent * 85,
                    height: 40,
                    marginBottom: width / 1080 * 334 * 0.2,
                    marginLeft: 10,
                    flexDirection: 'row',
                }}>

                    <TouchableOpacity
                        style={{
                            width: '20%',
                            height: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        onPress={props._openDrawer}
                    >
                        <Image style={{
                            width: 30,
                            height: 30,
                        }}
                               source={require('../../Image/Icons/iconDrawer.png')}
                               resizeMode={'contain'}
                        />

                    </TouchableOpacity>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#fff',
                            borderRadius: 25,
                            flexDirection: 'row',
                            paddingHorizontal: 15,
                            justifyContent: 'space-between',
                        }}>
                        <TextInput
                            style={{
                                color: '#000',
                                fontFamily: 'Myriad Pro',
                                fontSize: width * 0.035,
                                fontWeight: '400',
                                padding: 0,
                                flex: 1
                            }}
                            placeholder={props.placeHolder}
                            placeholderTextColor={'#b2b2b2'}
                            onChangeText={text => props._changeText(text)}
                            value={props.keySearch}
                            // editable={false}
                        />
                        <TouchableOpacity style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                        }} onPress={props._searchCourse}>
                            <Image style={{
                                width: 30,
                                height: 30,
                            }}
                                   source={require('../../Image/Icons/iconSearch.png')}
                                   resizeMode={'contain'}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
};
export default HeaderCategory;
