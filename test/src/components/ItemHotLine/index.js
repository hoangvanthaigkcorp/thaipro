import React from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
const {width, height} = Dimensions.get('screen');

const ItemHotLine = (props) => {
    return (
        <View style={{
            flexDirection: 'row',
            marginTop: height * 0.03,
            // alignItems: 'center',
            justifyContent: 'space-between',
            marginRight: width * 0.02,
        }}>
            <View style={{flexDirection: 'row', flex: 1, paddingRight: width * 0.03}}>
                <Image
                    source={require('../../Image/Icons/iconLocation.png')}
                    resizeMode={'contain'}
                    style={{
                        width: width * 0.03,
                        height: height * 0.03,
                        borderTopLeftRadius: width * 0.03,
                        borderBottomLeftRadius: width * 0.03,
                    }}
                />
                <Text style={{
                    color: '#000',
                    fontSize: 16,
                    marginLeft: width * 0.02,
                }}>{props.item.address + ':'}</Text>
            </View>
            <Text style={{color: '#0071bb', fontWeight: '700', fontSize: 16}}>{props.item.phone}</Text>
        </View>
    );
};

export default ItemHotLine;
