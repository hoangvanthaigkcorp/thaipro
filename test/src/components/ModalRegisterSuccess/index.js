import React from 'react';
import {View, Dimensions, TouchableWithoutFeedback, Image, Text} from 'react-native';

const {width, height} = Dimensions.get('window');

const ModalRegisterSuccess = (props) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => props.closeModal()}
        >
            <View style={{
                width,
                height,
                position: 'absolute',
                top: 0,
                zIndex: 1,
                backgroundColor: '#00000030',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={{
                    width: '74.5%',
                    height: '34.1%',
                    borderRadius: width * 0.07,
                    backgroundColor: '#fff',
                    alignItems: 'center',
                    paddingHorizontal: width * 0.15,
                    justifyContent: 'center',
                }}>
                    <Image
                        style={{
                            width: width * 0.21,
                            height: width * 0.21,
                            marginBottom: height * 0.03,
                        }}
                        source={require('../../Image/Icons/iconSuccess.png')}
                        resizeMode={'contain'}
                    />
                    <Text style={{color: '#0071bb', fontSize: width * 0.05, textAlign: 'center'}}>Đã đăng ký khóa học
                        thành công!</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ModalRegisterSuccess;
