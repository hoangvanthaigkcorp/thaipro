import React from 'react';
import {
    Dimensions,
    FlatList,
    Image,
    StatusBar,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    SafeAreaView
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

const ModalShowCategoryHome = (props) => {
    return (
        <View style={{
            backgroundColor: '#00000060',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: 2,
            alignItems: 'center',
        }}
        >
            <View style={{
                width: '100%',
                height: '100%',
                marginTop: HeightPercent * 18 + 100 + 10 * 2 + 30,
            }}>
                <TouchableWithoutFeedback onPress={props._openListCategory}>
                    <View
                        style={{
                            borderWidth: 1,
                            borderColor: '#5adcff',
                            borderRadius: width * 0.07,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingHorizontal: width * 0.05,
                            alignItems: 'center',
                            height: WidthPercent * 10,
                            backgroundColor: '#fff',
                            position: 'absolute',
                            top: WidthPercent * 5,
                            left: width * 0.19,
                            right: width * 0.19,
                        }}>
                        <Text style={{color: '#333333'}}>{props.titleCategory}</Text>
                        <Image
                            resizeMode="contain"
                            style={{
                                width: width * 0.05,
                                height: width * 0.05,
                                transform: [{rotate: '180deg'}],
                            }}
                            source={require('../../Image/Icons/iconDown.png')}
                        />
                    </View>
                </TouchableWithoutFeedback>

                <View
                    style={{
                        borderWidth: 1,
                        borderColor: '#5adcff',
                        borderRadius: width * 0.05,
                        paddingVertical: height * 0.01,
                        paddingHorizontal: width * 0.05,
                        position: 'absolute',
                        top: WidthPercent * 5 + WidthPercent * 10 + 5,
                        left: width * 0.19,
                        right: width * 0.19,
                        maxHeight: height * 0.3,
                        backgroundColor: '#fff',
                    }}>
                    <FlatList
                        data={props.itemCategory}
                        renderItem={({item}) => props._renderFlatList(item, 'category')}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        </View>
    );
};

export default ModalShowCategoryHome;
