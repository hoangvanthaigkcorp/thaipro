import React, {Component} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

export default function ItemClass({item, navigation}) {
    const imageDefault = "../../Image/image/ItemFlatList.png";

    return (
        <TouchableOpacity style={{flex: 1, flexDirection: 'row', marginVertical: HeightPercent, backgroundColor: '#fff', borderRadius: 10}}
            onPress={navigation}
                          onLongPress={navigation}
        >
            <Image style={{
                width: WidthPercent * 28,
                height: '100%',
                borderTopLeftRadius: 10,
                borderBottomLeftRadius: 10
            }}
                source={item.CourseImg ? { uri: item.CourseImg } : require(imageDefault) }
            />
            <View style={{flex: 1, padding: width * 0.01}}>
                <Text style={{
                    color: '#0e0d42',
                    fontFamily: 'Myriad Pro',
                    fontSize: width * 0.035,
                    fontWeight: '700',
                }}>{item.CourseName}</Text>
                <View style={{flexDirection: 'row', paddingVertical: HeightPercent}}>
                    <Text style={{
                        color: '#989898',
                        fontFamily: 'Arial',
                        fontSize: width * 0.03,
                        fontWeight: '400',
                        flex: 4
                    }}>Thời gian học:</Text>
                    <Text style={{
                        color: '#989898',
                        fontFamily: 'Arial',
                        fontSize: width * 0.03,
                        fontWeight: '400',
                        flex: 3,
                    }}>{item.TotalDuration + `'`}</Text>
                </View>
                <View style={{flexDirection: 'row', paddingVertical: HeightPercent}}>
                    <Text style={{
                        color: '#989898',
                        fontFamily: 'Arial',
                        fontSize: width * 0.03,
                        fontWeight: '400',
                        flex: 4
                    }}>Truy cập lần cuối:</Text>
                    <Text style={{
                        color: '#989898',
                        fontFamily: 'Arial',
                        fontSize: width * 0.03,
                        fontWeight: '400',
                        flex: 3
                    }}>{item.LastAccessedDateTime}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}
