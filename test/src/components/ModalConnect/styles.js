import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

export default StyleSheet.create({
    viewModalContainer: {
        position: 'absolute',
        top:0,
        width,
        height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000070'
    },
    viewModalContent: {
        width: '82%',
        height: '20%',
        backgroundColor: '#fdf1f6',
        borderRadius: width * 0.08,
        paddingVertical: width * 0.06,
        paddingHorizontal: width * 0.1,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    titleNotification: {
        color: '#686868',
        fontSize: width * 0.045,
        textAlign: 'center'
    },
    buttonToLogin: {
        paddingHorizontal: width * 0.08,
        paddingVertical: height * 0.012,
        backgroundColor: '#033b6d',
        borderRadius: width * 0.08
    },
    titleBack: {
        color: '#fff',
        fontSize: width * 0.045,
        fontWeight: '700'
    }
})
