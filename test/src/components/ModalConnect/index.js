import React from 'react';
import {Modal, View, Text, TouchableOpacity, StatusBar} from 'react-native';
import styles from './styles';

const ModalConnect = ({_onClose}) => {
    return (
            <View style={styles.viewModalContainer}>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
                <View style={styles.viewModalContent}>
                    <Text style={styles.titleNotification}>
                        Vui lòng kiểm tra kết nối Internet!
                    </Text>
                    <TouchableOpacity
                        style={styles.buttonToLogin}
                        onPress={_onClose}>
                        <Text style={styles.titleBack}>Đồng ý</Text>
                    </TouchableOpacity>
                </View>
            </View>
    );
};

export default ModalConnect;
