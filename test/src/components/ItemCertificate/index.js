import React, {Component} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

export default function ItemCertificate({item,key}) {
    return (
        <View style={{
            flex:1,
            flexDirection:'row',
            alignItems:'center',
            paddingRight:20
        }}
              key={key}
        >
            <ImageBackground style={{
                width: 25,
                height: 25,
            }}
                   source={require('../../Image/image/Circle.png')}
                   resizeMode={'contain'}
            >
                <Text style={{
                    color: '#0e0d42',
                    fontFamily: 'Myriad Pro',
                    fontSize: 16,
                    fontWeight: '400',
                    marginLeft:15
                }}>{key}</Text>
            </ImageBackground>
            <Text style={{
                color: '#0e0d42',
                fontFamily: 'Myriad Pro',
                fontSize: 16,
                fontWeight: '400',
                marginLeft:15
            }}>{item.title} {key}</Text>
        </View>
    );
}
