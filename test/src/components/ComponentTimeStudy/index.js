import React, {useState, useEffect} from 'react';
import {Dimensions, Text, TouchableOpacity, View, Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('screen');
const shadow = {
    shadowColor: '#000',
    shadowOffset: {width: 10, height: 10},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
};

const TimeStudyHome = (props) => {
    const userLearningSummary = props.userLearningSummary;

    const _getTimeString = (minutes) => {
        const h = parseInt(minutes%(60*60)/60) || 0;
        const m = parseInt(minutes%60) || 0;

        return ((h<10?'0'+h:h) + ':' + (m<10?'0'+m:m));
    };

    const _getStatus = (status) => {
        switch (status) {
            case "new":
                return "Chưa bắt đầu";
            case "in_process":
                return "Đang học";
            case "completed":
                return "Đã hoàn thành";
            default:
                return "Chưa bắt đầu";
        }
    };

    return (
        <View style={[shadow, {
            backgroundColor: '#fff',
            marginHorizontal: width * 0.06,
            borderRadius: 5,
            paddingHorizontal: width * 0.04,
            paddingTop: height * 0.01,
            justifyContent: 'space-between',
            paddingBottom: height * 0.02,
            marginVertical: 10,
            height: 100,
        }]}
        >
            <View style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-around',
                alignItems: 'center',
            }}>
                <Text style={{textAlign: 'center', width: '30%', color: '#0E0D42', fontSize: 12}}>Tổng thời gian học yêu
                    cầu</Text>
                <Text style={{textAlign: 'center', width: '30%', color: '#0E0D42', fontSize: 12}}>Thời gian học của bạn</Text>
                <Text style={{textAlign: 'center', width: '30%', color: '#0E0D42', fontSize: 12}}>Trạng thái chung</Text>
            </View>

            <View style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-around',
                alignItems: 'center',
            }}>
                <Text style={{textAlign: 'center', width: '30%', fontSize: 26, color: '#00CDE8'}}>{_getTimeString(userLearningSummary.total_time_required)}</Text>
                <Text style={{textAlign: 'center', width: '30%', fontSize: 26, color: '#00CDE8'}}>{_getTimeString(userLearningSummary.total_course_required)}</Text>
                <LinearGradient colors={['#4327c2', '#00CDE8']}
                                style={{width: '30%', borderRadius: 5, paddingVertical: height * 0.01}}
                                start={{x: 0, y: 0}} end={{x: 1, y: 0}}>
                    <Text style={{textAlign: 'center', color: '#fff'}}>{_getStatus(props.userLearningSummary.learning_status)}</Text>
                </LinearGradient>
            </View>
        </View>
    );
};

export default TimeStudyHome;
