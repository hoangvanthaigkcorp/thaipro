import React, { PureComponent } from 'react';
import {View, Text, ActivityIndicator, StatusBar} from 'react-native';
import styles from "./Loading.Style";

class LoadingScreen extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
                <ActivityIndicator size="large" color="#4F6AEF" />
                <View style={styles.wrapText}>
                    <Text style={styles.textloading}>Hãy chờ trong giây lát</Text>
                </View>
            </View>
        );
    }
}

export default LoadingScreen;
