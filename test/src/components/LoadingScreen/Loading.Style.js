import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        flex: 1,
        //backgroundColor: "rgba(255.0,255.0,255.0,0.6)",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 100000
      },
      wrapText: {
        justifyContent: "center",
        alignItems: "center",
        //bottom: height / 3
      },
      textloading: {
          fontSize: 14,
          color: "black",
          fontWeight: "400"
      }
})