import React, {useState, useEffect} from 'react';
import {Dimensions, View, ImageBackground, Text, TouchableOpacity, Image} from 'react-native';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../redux/actions/authLoginAction';
import { useSelector } from 'react-redux';
import API from '../../api/APIConstant';
import axios from 'axios';
import { useIsDrawerOpen } from '@react-navigation/drawer';

const {width, height} = Dimensions.get('screen');
const linkIcon = '../../Image/Icons/';

const CustomDrawer = (props) => {
    const {og_data} = useSelector(state => state.ogData);
    const {userInfo} = useSelector(state => state.infoUser);
    const dispatch = useDispatch();
    const isDrawerOpen = useIsDrawerOpen();
    const [name, setName] = useState(null);

    useEffect(() => {
        if(isDrawerOpen){
            _getUserProfile();
        }
    }, [isDrawerOpen]);

    const _getUserProfile = async () => {
        // let userLogonID = "100005";
        const url = API.baseurl + API.userProfile + userInfo.username + API.og_code;
        try {
            const res = await axios({ method: 'get', url: url });
            if (res.data.status) {
                setName(res.data.user_data.full_name);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const renderItemDrawer = (icon, title, navigation) => {
        return (
            <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: height * 0.02}}
                              onPress={navigation}
            >
                <View style={{flex: 2, alignItems: 'flex-end'}}>
                    <Image
                        source={icon}
                        style={{width: width * 0.09, height: width * 0.09, alignItems: 'center', justifyContent: 'center'}}
                        resizeMode={'contain'}
                    />
                </View>
                <View style={{flex: 3, paddingLeft: width * 0.05, paddingRight: width * 0.04}}>
                    <Text style={{color: '#0e0d42', fontWeight: '700', fontSize: 16}}>{title}</Text>
                </View>
            </TouchableOpacity>
        );
    };

    const _logout = () => {
        let dataLogin = {};
        dataLogin['isLogin'] = false;
        dispatch(loginSuccess(dataLogin));
    };
    return (
        <View style={{flex: 1}}>
            <ImageBackground
                source={require('../../Image/image/backgroundDrawer.png')}
                style={{width: '100%', height: height * 0.45, alignItems: 'center', justifyContent: 'center'}}
                // resizeMode={'contain'}
            >
                <View style={{width: width * 0.26, height: width * 0.26, backgroundColor: '#fff', borderRadius: 100, alignItems: 'center', justifyContent: 'center'}}>
                    <Image
                        style={{
                            width: width * 0.2,
                            height: width * 0.2,
                        }}
                        source={{uri: og_data.logo_url}}
                        resizeMode={'contain'}
                    />
                </View>
                <Text style={{color: '#fff', fontWeight: '700', marginTop: height * 0.02, fontSize: 20}}>{name}</Text>
            </ImageBackground>

            {renderItemDrawer(require('../../Image/Icons/' + 'iconFile.png'), 'Hồ sơ', () => props.navigation.navigate('Profile'))}
            {renderItemDrawer(require('../../Image/Icons/' + 'iconCertificate.png'), 'Điều kiện xét cấp chứng chỉ', () => props.navigation.navigate('RegisterCertificate'))}
            {renderItemDrawer(require('../../Image/Icons/' + 'iconLogout.png'), 'Đăng xuất', _logout)}
        </View>
    );
};

export default CustomDrawer;
