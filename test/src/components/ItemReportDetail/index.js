import React, {Component} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

const ItemReportDetail = ({item}) => {
    return (
        <View style={{paddingTop:HeightPercent*1.5,flexDirection:'row',paddingHorizontal:15}}>
            <Text style={{
                color: '#4d4d4d',
                fontFamily: 'Arial',
                fontSize: 16,
                fontWeight: '400',
                flex:1
            }}>{item.title}</Text>
            <Text style={{
                color: '#0071bb',
                fontFamily: 'Arial',
                fontSize: 16,
                fontWeight: '400',
                flex:1
            }}>{item.detail}</Text>
        </View>
    )
};
export default ItemReportDetail
