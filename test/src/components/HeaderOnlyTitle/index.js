import React, {Component} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

const HeaderOnlyTitle = ({title, Back, navigation}) => {
    return (
        <View style={{width:width, height:HeightPercent*18}}>
            <ImageBackground style={{
                flex:1,
            }}
                             source={require('../../Image/image/HeaderCategory.png')}
                             resizeMode={'stretch'}
            >
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor="transparent" translucent={true}/>
                <View style={{
                    width:width,
                    height:40,
                    marginTop:HeightPercent*8,
                    // flexDirection:'row',
                    alignItems:'center'
                }}>
                    {Back && <TouchableOpacity style={{
                        position: 'absolute',
                        left: 20,
                    }}
                        onPress={navigation}
                        >
                        <Image style={{
                            width: 25,
                            height: 25,
                        }}
                               source={require('../../Image/Icons/iconback.png')}
                               resizeMode={'contain'}
                        />
                    </TouchableOpacity>}
                    <Text style={{
                        color: '#ffffff',
                        fontFamily: 'Myriad Pro',
                        fontSize: 21,
                        fontWeight: '700',
                    }}>{title}</Text>
                </View>
            </ImageBackground>
        </View>
    );
};
export default HeaderOnlyTitle;
