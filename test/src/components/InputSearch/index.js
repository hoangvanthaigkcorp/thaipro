import React from 'react';
import {Image, TextInput, TouchableOpacity, View} from 'react-native';

const InputSearch = (props) => {
    return (
        <View style={{
            flex: 1,
            backgroundColor: '#fff',
            borderRadius: 25,
            flexDirection: 'row',
            paddingHorizontal: 15,
            justifyContent: 'space-between',
        }}>
            <TextInput
                style={{
                    color: '#000',
                    fontFamily: 'Myriad Pro',
                    fontSize: 16,
                    fontWeight: '400',
                    padding: 0,
                }}
                placeholder={props.placeHolder}
                placeholderTextColor={'#b2b2b2'}
                autoFocus={true}
            />
            <TouchableOpacity style={{
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Image style={{
                    width: 30,
                    height: 30,
                }}
                       source={require('../../Image/Icons/iconSearch.png')}
                       resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View>
    );
};

export default InputSearch;
