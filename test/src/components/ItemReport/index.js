import React, { Component } from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';
import ProgressCircle from 'react-native-progress-circle'
const { width, height } = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;
import Stars from 'react-native-stars';

const ItemReport = (props) => {
    const _onNavigate = () => {
        props.onNavigate
    };
    const item = props.item;
    return (
        <TouchableOpacity
            onPress={_onNavigate}
            style={{
                width: width - 40,
                flexDirection: 'row',
                marginVertical: 10,
                height: HeightPercent * 12
            }}>
            <View style={{ width: WidthPercent * 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{
                    width: 25,
                    height: 25,
                }}
                    source={require('../../Image/Icons/fullstar.png')}
                    resizeMode={'contain'}
                />
            </View>
            <View style={{
                width: WidthPercent * 70,
                backgroundColor: '#fff',
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 5,
                paddingHorizontal: 10
            }}>
                <Text style={{
                    color: '#000000',
                    fontFamily: 'Myriad Pro',
                    fontSize: 18,
                    fontWeight: '400',
                    flex: 3
                }}>{`${item.CourseName}(${item.CourseNumber})`}</Text>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <ProgressCircle
                        percent={item.score}
                        radius={25}
                        borderWidth={8}
                        color="#00cde8"
                        shadowColor="#f1f1f1"
                        bgColor="#fcfcfc"
                    >
                        <Text style={{ fontSize: 16 }}>{`${item.score}%`}</Text>
                    </ProgressCircle>
                </View>
            </View>
            <View style={{ width: WidthPercent * 10, justifyContent: 'center' }}>
                <Image style={{
                    width: 25,
                    height: 25,
                }}
                    source={require('../../Image/Icons/arowright.png')}
                    resizeMode={'contain'}
                />
            </View>
        </TouchableOpacity>
    );
}
export default ItemReport;
