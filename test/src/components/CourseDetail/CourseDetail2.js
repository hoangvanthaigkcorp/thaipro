import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions,Image} from 'react-native';
import { RNCamera } from 'react-native-camera';
import HeaderOnlyTitle from '../../components/HeaderOnlyTitle';
import LinearGradient from "react-native-linear-gradient";
const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

class CourseDetail2 extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <HeaderOnlyTitle title={'Chụp ảnh thẻ'} Back={false}/>
                <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    locations={[0, 1]}
                    colors={['#4327c2', '#00cde8']}
                    style={{
                        borderRadius: 10,
                        marginTop: 50,
                        width: WidthPercent * 86,
                        height: WidthPercent*43 +10,
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding:10,
                    }}>
                    <View style={{width:'100%',height:'100%',overflow: 'hidden'}}>
                        <Image
                            source={{ uri: `data:image/png;base64,${this.props.Image}`}}
                            style={{ height: '100%', width: '100%' }}
                        />
                    </View>
                </LinearGradient>
                <View style={{
                    width: WidthPercent * 86,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    marginTop:HeightPercent*5,
                    shadowColor: "#000",
                    padding: 10,
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.37,
                    shadowRadius: 7.49,

                    elevation: 12,
                }}>
                    <Text style={{
                        color: '#0071bb',
                        fontFamily: 'Myriad Pro',
                        fontSize: 18,
                        fontWeight: '400',
                    }}> Nếu ảnh đã nhìn rõ mặt, hãy click tiếp tục, nếu chưa, vui lòng click chụp lại. </Text>
                </View>
                <View style={{
                    width,
                    backgroundColor:'#fff',
                    position:'absolute',
                    bottom:0,
                    paddingVertical: 15,
                    justifyContent:'space-around',
                    flexDirection: 'row'
                }}>
                    <TouchableOpacity  onPress={()=>this.props.TakePictureAgain()}>
                        <LinearGradient
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}
                            locations={[0, 1]}
                            colors={['#4327c2', '#00cde8']}
                            style={{
                                width: WidthPercent * 40,
                                height: HeightPercent*6,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius:HeightPercent * 3
                            }}>
                            <Text style={{
                                color: '#ffffff',
                                fontFamily: 'Myriad Pro',
                                fontSize: 18,
                                fontWeight: '400',
                            }}>Chụp lại</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.SendImage()}>
                        <LinearGradient
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}
                            locations={[0, 1]}
                            colors={['#4327c2', '#00cde8']}
                            style={{
                                width: WidthPercent * 40,
                                height: HeightPercent*6,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius:HeightPercent * 3
                            }}>
                            <Text style={{
                                color: '#ffffff',
                                fontFamily: 'Myriad Pro',
                                fontSize: 18,
                                fontWeight: '400',
                            }}>Gửi ảnh</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                </View>
                {/*<View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>*/}
                {/*    <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>*/}
                {/*        <Text style={{ fontSize: 14 }}> SNAP </Text>*/}
                {/*    </TouchableOpacity>*/}
                {/*</View>*/}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
        alignItems: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: HeightPercent * 25,
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
export default CourseDetail2
