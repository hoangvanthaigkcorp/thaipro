import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions} from 'react-native';
import { RNCamera } from 'react-native-camera';
import HeaderOnlyTitle from '../../components/HeaderOnlyTitle';
import LinearGradient from "react-native-linear-gradient";
import CourseDetail5 from './CourseDetail5';
const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;
import { WebView } from 'react-native-webview';

class CourseDetail1 extends PureComponent {

    render() {
        return (

            <View style={styles.container}>
                    <View style={styles.container}>
                        <HeaderOnlyTitle title={'Chụp ảnh thẻ'} Back={false}/>
                        <LinearGradient
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}
                            locations={[0, 1]}
                            colors={['#4327c2', '#00cde8']}
                            style={{
                                borderRadius: 10,
                                marginTop: 50,
                                width: WidthPercent * 86,
                                height: WidthPercent*43 +10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding:10,
                            }}>
                            <View style={{width:'100%',height:'100%',overflow: 'hidden'}}>
                                <RNCamera
                                    ref={ref => {
                                        this.camera = ref;
                                    }}
                                    ratio={"4:3"}
                                    style={styles.preview}
                                    type={RNCamera.Constants.Type.back}
                                    flashMode={RNCamera.Constants.FlashMode.off}
                                    androidCameraPermissionOptions={{
                                        title: 'Permission to use camera',
                                        message: 'We need your permission to use your camera',
                                        buttonPositive: 'Ok',
                                        buttonNegative: 'Cancel',
                                    }}
                                    androidRecordAudioPermissionOptions={{
                                        title: 'Permission to use audio recording',
                                        message: 'We need your permission to use your audio',
                                        buttonPositive: 'Ok',
                                        buttonNegative: 'Cancel',
                                    }}
                                >
                                    {({ status}) => {
                                        if (status === 'NOT_AUTHORIZED'){
                                            this.setState({status:true})
                                        }

                                    }}
                                </RNCamera>
                            </View>
                        </LinearGradient>
                        <View style={{
                            width: WidthPercent * 86,
                            backgroundColor:'#fff',
                            borderRadius:10,
                            marginTop:HeightPercent*4,
                            shadowColor: "#000",
                            padding: 10,
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.37,
                            shadowRadius: 7.49,
                            elevation: 12,
                        }}>
                            <Text style={{
                                color: '#0071bb',
                                fontFamily: 'Myriad Pro',
                                fontSize: 18,
                                fontWeight: '400',
                            }}> Vui lòng đưa thẻ thành viên của bạn vào khung hình và căn chỉnh sao cho toàn bộ thẻ nằm gọn trong khung. </Text>
                        </View>
                        <View style={{
                            width,
                            backgroundColor:'#fff',
                            position:'absolute',
                            bottom:0,
                            paddingVertical: 15,
                            alignItems:'center'
                        }}>
                            <TouchableOpacity onPress={this.takePicture.bind(this)}>
                                <LinearGradient
                                    start={{x: 0, y: 0}}
                                    end={{x: 1, y: 0}}
                                    locations={[0, 1]}
                                    colors={['#4327c2', '#00cde8']}
                                    style={{
                                        width: WidthPercent * 60,
                                        height: HeightPercent*6,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius:HeightPercent * 3
                                    }}>
                                    <Text style={{
                                        color: '#ffffff',
                                        fontFamily: 'Myriad Pro',
                                        fontSize: 18,
                                        fontWeight: '400',
                                    }}>Chụp</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
            </View>
        );
    }

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.props.TakePicture(data.base64)
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
        alignItems: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: HeightPercent * 25,
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
export default CourseDetail1
