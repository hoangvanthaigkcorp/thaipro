import React, { PureComponent } from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Dimensions, Image} from 'react-native';
import { RNCamera } from 'react-native-camera';
import HeaderOnlyTitle from '../../components/HeaderOnlyTitle';
import LinearGradient from "react-native-linear-gradient";
const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

class CourseDetail5 extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <HeaderOnlyTitle title={'Chụp ảnh chân dung'} Back={false}/>
                <Image style={{
                    width: WidthPercent*50,
                    height: WidthPercent*50,
                    marginTop:HeightPercent*8,
                }}
                       source={require('../../Image/image/Camera.png')}
                       resizeMode={'contain'}
                />
                <View style={{
                    width: WidthPercent * 86,
                    backgroundColor:'#fff',
                    borderRadius:10,
                    shadowColor: "#000",
                    padding: 10,
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.37,
                    shadowRadius: 7.49,

                    elevation: 12,
                }}>
                    <Text style={{
                        color: '#0071bb',
                        fontFamily: 'Myriad Pro',
                        fontSize: 18,
                        fontWeight: '400',
                    }}>Vui lòng đóng App, sau đó khởi động lại khóa học và click “ Allow ” hoặc “Đồng ý” khi được hỏi quyền sử dụng camera. </Text>
                </View>

            </View>
        );
    }

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.props.TakePictureProfile(data.base64);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
        alignItems: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: HeightPercent * 25,
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
export default CourseDetail5
