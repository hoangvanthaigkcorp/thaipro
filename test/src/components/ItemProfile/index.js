import React, {Component} from 'react';
import {
    StatusBar,
    Image,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

export default function ItemProfile({item}) {
    return (
        <View style={{
            width:width-20,
            flexDirection:'row',
            justifyContent:'space-between',
            paddingHorizontal:5,
            paddingVertical:15,
            borderBottomWidth:1,
            borderColor: '#e5e5e5',
            borderStyle: 'solid',
        }}
        >
            <Text style={{
                color: '#0e0d42',
                fontFamily: 'Myriad Pro',
                fontSize: 16,
                fontWeight: '700',
            }}>{item.title}</Text>
            <Text style={{
                color: '#0e0d42',
                fontFamily: 'Myriad Pro',
                fontSize: 16,
                fontWeight: '400',
            }}>{item.detail}</Text>

        </View>
    );
}
