import React from 'react';
import {View, Dimensions, TouchableWithoutFeedback, TouchableOpacity, Text} from 'react-native';

const {width, height} = Dimensions.get('window');

const ModalLogin = (props) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => props.closeModal()}
        >
            <View style={{
                width,
                height,
                position: 'absolute',
                top: 0,
                zIndex: 1,
                backgroundColor: '#00000030',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={{
                    width: '74.5%',
                    // height: '34.1%',
                    borderRadius: width * 0.07,
                    backgroundColor: '#fff',
                }}>
                    <View
                        style={{flex: 1, alignItems: 'center', justifyContent: 'space-between', padding: width * 0.07}}>
                        <Text style={{color: '#0e0d42', fontSize: width * 0.05, textAlign: 'center'}}>Hệ thống ghi nhận
                            tài khoản
                            này hiện đang được đăng nhập
                            ở một thiết bị khác.
                        </Text>
                        <Text style={{color: '#0e0d42', fontSize: width * 0.05, textAlign: 'center'}}>Bạn có muốn đăng
                            xuất khỏi
                            thiết bị đó hay không?
                        </Text>
                    </View>
                    <View style={{flexDirection: 'row', borderTopWidth: 1, borderColor: '#b2b2b2', height: '24.7%'}}>
                        <TouchableOpacity style={{
                            flex: 1,
                            borderRightWidth: 1,
                            borderColor: '#b2b2b2',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                                          onPress={() => props.closeModal()}
                        >
                            <Text style={{color: '#808080', fontSize: width * 0.05}}>Thoát</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
                            onPress={() => {
                                props.submit();
                                props.closeModal();
                            }}
                            onLongPress={() => {
                                props.submit();
                                props.closeModal();
                            }}
                        >
                            <Text style={{color: '#0071bb', fontSize: width * 0.05}}>Có</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default ModalLogin;
