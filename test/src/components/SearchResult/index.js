import React from 'react';
import {Dimensions, FlatList, View, Text, TouchableWithoutFeedback} from 'react-native';

const {width, height} = Dimensions.get('window');

const renderValue = ({item}) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => alert(item)}
        >
            <View style={{height: height * 0.05, justifyContent: 'center'}}>
                <Text style={{paddingHorizontal: 15, color: '#4d4d4d', fontSize: width * 0.04}}>{item}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
};
const SearchResult = (props) => {
    return (
        <View style={{backgroundColor: '#fff', borderRadius: width * 0.04}}>
            <FlatList
                data={props.itemValueSearch}
                keyExtractor={(item, index) => {
                    return item.toString() + index.toString();
                }}
                renderItem={renderValue}
            />
        </View>
    );
};

export default SearchResult;
