
import React, {Component} from 'react';
import {
    Modal,
    Text,
    TouchableOpacity,
    View,
    Dimensions, StatusBar, Image,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;

export default function ModalChoiceItem({Data,modalVisible,PressClose}) {
    return (
            <Modal
                animationType="none"
                transparent={true}
                visible={modalVisible}
            >
                <TouchableOpacity style={{flex:1,backgroundColor:'#00000060',alignItems:'center'}}
                onPress={()=>PressClose()}
                >
                    <View style={{
                        width:(width - 40)*0.7*0.8,
                        height: 40,
                        backgroundColor: '#FFFFFF',
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: '#5adcff',
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 15,
                        justifyContent: 'space-between',
                        marginTop:HeightPercent*30 -50
                    }}
                    >
                        <Text style={{
                            color: '#333333',
                            fontFamily: 'Myriad Pro',
                            fontSize: 18,
                            fontWeight: '400',
                        }}>Chọn danh mục</Text>
                        <Image style={{
                            width: 20,
                            height: 15,
                        }}
                               source={require('../../Image/Icons/Arow.png')}
                               resizeMode={'contain'}
                        />
                    </View>
                    <View style={{
                        width:(width - 40)*0.7*0.8,
                        height:200,
                        marginTop:10,
                        backgroundColor:'#fff',
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: '#5adcff',
                    }}>

                        {Data.map(e=>(
                            <Text>{e.title}</Text>
                        ))}
                    </View>
                </TouchableOpacity>

            </Modal>
    );
}
