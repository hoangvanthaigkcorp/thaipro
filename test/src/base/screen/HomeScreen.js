import React, {useState, useEffect} from 'react';
import {
    View,
    Text,
    Dimensions,
    Image,
    TouchableWithoutFeedback,
    FlatList, ScrollView, RefreshControl,
    Alert,
    StatusBar,
    SafeAreaView ,
} from 'react-native';

const {width, height} = Dimensions.get('screen');
const WidthPercent = width / 100;
const HeightPercent = height / 100;
import HeaderCategory from '../../components/HeaderCategory';
import ItemCourse from '../../components/ComponentCourseHome';
import ItemHotLine from '../../components/ItemHotLine';
import TimeStudyHome from '../../components/ComponentTimeStudy';
import API from '../../api/APIConstant';
import axios from 'axios';
import LoadingScreen from '../../components/LoadingScreen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useSelector, useDispatch} from 'react-redux';
import {showHotline} from '../../redux/actions/showHotlineAction';

var Width = Dimensions.get("window").width;
var Height = Dimensions.get('window').height;
StatusBar.setHidden(false, 'slide');


export default function HomeScreen({ navigation }, props) {

const {userInfo} = useSelector(state => state.infoUser);
const shadow = {
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 8,
};
const [refreshing, setRefreshing] = useState(false);
const [scrollEnabled, setScrollEnable] = useState(true);
const [refreshEnable, setRefreshEnable] = useState(true);
const [userLearningSummary, setUserLearningSummary] = useState({});
const [itemCourse, setItemCourse] = useState([]);
const [itemCategory, setItemCategory] = useState([]);
const [itemProgramme, setItemProgramme] = useState([]);
const [numberCourse, setNumberCourse] = useState(0);
const [showCategory, setShowCategory] = useState(false);
const [showProgramme, setShowProgramme] = useState(false);
const [isLoading, setIsLoading] = useState(false);
const [categoryId, setCategoryId] = useState('14');
const [curriculumID, setCurriculumID] = useState('17');
const [keySearch, setKeySearch] = useState('');
const {og_data} = useSelector(state => state.ogData);
const [titleCategory, setTitleCategory] = useState('Chọn danh mục');
const [titleCurriculum, setTitleCurriculum] = useState('Chọn chương trình');
const [itemAddressHotLine] = useState([
    og_data,
]);
const dispatch = useDispatch();
const {show_hotline} = useSelector(state => state.hotline);

useEffect(() => {
    _getCategories();
}, [userInfo]);

useEffect(() => {
    _getCurriculums();
}, [userInfo, categoryId]);

// useEffect(() => {
//     _getItemCourse();
// }, [userInfo]);

useEffect(() => {
    _getUserLearningSummary();
}, [userInfo]);

useEffect(() => {
    if (keySearch) {
        _searchCourse();
    } else {
        _filterCourseByCategoryOrProgramme();
    }
}, [categoryId, curriculumID]);

const _getCategories = async () => {
    const url = API.baseurl + API.categories + API.og_code;
    try {
        const res = await axios({method: 'get', url: url});
        if (res.data.status) {
            let data = {};
            data['CategoryID'] = '';
            data['CategoryName'] = 'Tất cả';
            let dataCategory = res.data.data_category;
            dataCategory.unshift(data);
            setItemCategory(dataCategory);
        }
    } catch (error) {
        console.log(error);
    }
};

const _getCurriculums = async () => {
    const url = API.baseurl + API.curriculums + API.og_code + '&' + API.categoryID(categoryId) ;
    try {
        const res = await axios({method: 'get', url: url});
        if (res.data.status) {
            let data = {};
            data['CurriculumID'] = '';
            data['CurriculumName'] = 'Tất cả';
            let dataCurriculum = res.data.data_curriculum;
            dataCurriculum.unshift(data);
            setItemProgramme(dataCurriculum);
        }
    } catch (error) {
        console.log(error);
    }
};

const _getUserLearningSummary = async () => {
    // let userLogonID = 100005;
    const url = API.baseurl + API.userLearningSummary + userInfo.username + API.og_code;
    try {
        const res = await axios({method: 'get', url: url});
        let data = res.data;
        if (data.status) {
            setUserLearningSummary(data);
        }
    } catch (error) {
        console.log(error);
    }
};

const _searchCourse = async () => {
    setIsLoading(true);
    const url = API.baseurl + API.searchCourseHome + 'UserLogonID=' + userInfo.username + API.og_code + '&' + API.categoryID(categoryId) + '&' + API.curriculumID(curriculumID) + '&search_value=' + keySearch;
    try {
        const res = await axios({method: 'get', url: url});
        await setItemCourse(res.data.list_course);
        await setNumberCourse(res.data.list_course.length);
        setIsLoading(false);
    } catch (error) {
        console.log(error);
        await setItemCourse([]);
        await setNumberCourse(0);
        Alert.alert('Thông báo', 'Không tải được danh sách khóa.', [
                {text: 'Đồng ý'},
            ],
            {cancelable: false});
        setIsLoading(false);
    } finally {
        setTimeout(() => {
            setIsLoading(false);
        }, 30000);
    }
};

const _changeText = (text) => {
    setKeySearch(text);
};

const _setFilter = (item, type) => {
    if (type === 'category') {
        let title = item.CategoryID ? item.CategoryName : 'Chọn danh mục';
        setCategoryId(item.CategoryID);
        setTitleCategory(title);
        console.log(title);
        setShowCategory(false);
        setScrollEnable(true);
        setRefreshEnable(true);
        setCurriculumID('');
        setTitleCurriculum('Chọn chương trình');
    } else {
        let title = item.CurriculumID ? item.CurriculumName : 'Chọn chương trình';
        setCurriculumID(item.CurriculumID);
        setTitleCurriculum(title);
        setShowProgramme(false);
        setScrollEnable(true);
        setRefreshEnable(true);
    }
};

const _renderFlatList = (item, type) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => {
                _setFilter(item, type);
            }}
        >
            <Text style={{color: '#666666', marginVertical: 5, width: '100%'}}>
                {type === 'category' ? item.CategoryName : item.CurriculumName}
            </Text>
        </TouchableWithoutFeedback>
    );
};

const _openListCategory = () => {
    if (showProgramme) {
        setShowProgramme(false);
        setScrollEnable(true);
        setRefreshEnable(true);
    } else {
        setShowCategory(!showCategory);
        setScrollEnable(!scrollEnabled);
        setRefreshEnable(!refreshEnable);
    }
};

const _openListProgramme = () => {
    if (showCategory) {
        setShowCategory(false);
        setScrollEnable(true);
        setRefreshEnable(true);
    } else {
        setShowProgramme(!showProgramme);
        setScrollEnable(!scrollEnabled);
        setRefreshEnable(!refreshEnable);
    }
};

const _openDrawer = () => {
    props.navigation.openDrawer();
};

const _toCourseInformation = (CourseId) => {
    props.navigation.navigate('CourseInformation', {register: false, CourseId, _onRefresh});
};

const _filterCourseByCategoryOrProgramme = async () => {
    setIsLoading(true);
    const url = API.baseurl + API.apiUserTrainingPlan + '?' + 
    // API.userLogonID(userInfo.username) 
    'UserLogonID=100005'
    + API.og_code + (categoryId && '&' + API.categoryID(categoryId)) + (curriculumID && '&' + API.curriculumID(curriculumID));
    try {
        const res = await axios({method: 'get', url});
        await setItemCourse(res.data.list_course);
        await setNumberCourse(res.data.list_course.length);
        setIsLoading(false);
    } catch (e) {
        console.log(e);
        await setItemCourse([]);
        await setNumberCourse(0);
        Alert.alert('Thông báo', 'Không tải được danh sách khóa học.', [
                {text: 'Đồng ý'},
            ],
            {cancelable: false});
        setIsLoading(false);
    } finally {
        setTimeout(() => {
            setIsLoading(false);
        }, 30000);
    }
};

const _onRefresh = async () => {
    _getUserLearningSummary();
    _getCategories();
    _getCurriculums();
    if (keySearch) {
        _searchCourse();
    } else {
        _filterCourseByCategoryOrProgramme();
    }
    setRefreshing(false);
};

return (
        <View style={{backgroundColor: '#F0F0F0', flex: 1, zIndex: 1}}>
            <View style={{flex: 1}}>
                {HeaderCategory({
                    page: 'Home',
                    placeHolder: 'Tìm kiếm khóa học...',
                    _openDrawer,
                    _changeText,
                    _searchCourse,
                    keySearch: keySearch,
                })}

                <ScrollView contentContainerStyle={{flexGrow: 1}}
                            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={_onRefresh}
                                                            enabled={refreshEnable}/>}
                            scrollEnabled={scrollEnabled}
                >
                    {TimeStudyHome({showCategory, showProgramme, userLearningSummary})}

                    {!show_hotline ? (
                        <View style={{flex: 1}}>
                            {
                                showCategory && itemCategory.length > 0 &&
                                <SafeAreaView
                                    style={{
                                        ...shadow,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        borderRadius: width * 0.05,
                                        paddingVertical: height * 0.01,
                                        paddingHorizontal: width * 0.05,
                                        position: 'absolute',
                                        top: WidthPercent * 5 + WidthPercent * 10 + 5 + 30,
                                        left: width * 0.19,
                                        right: width * 0.19,
                                        maxHeight: height * 0.3,
                                        backgroundColor: '#fff',
                                        zIndex: 4,

                                    }}>
                                    <FlatList
                                        data={itemCategory}
                                        renderItem={({item}) => _renderFlatList(item, 'category')}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </SafeAreaView>
                            }

                            {
                                showProgramme && itemProgramme.length > 0 &&
                                <View
                                    style={{
                                        ...shadow,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        borderRadius: width * 0.05,
                                        paddingVertical: height * 0.01,
                                        paddingHorizontal: width * 0.05,
                                        position: 'absolute',
                                        top: WidthPercent * 5 + WidthPercent * 10 + WidthPercent * 3 + WidthPercent * 10 + 10 + 30,
                                        left: width * 0.19,
                                        right: width * 0.19,
                                        maxHeight: height * 0.3,
                                        backgroundColor: '#fff',
                                        zIndex: 4,
                                    }}>

                                    <FlatList
                                        data={itemProgramme}
                                        renderItem={({item}) => _renderFlatList(item, 'curriculum')}
                                        keyExtractor={(item, index) => index.toString()}
                                    />

                                </View>
                            }
                            <Text
                                style={{
                                    fontWeight: '700',
                                    fontSize: 16,
                                    color: '#0E0D42',
                                    marginLeft: width * 0.09,
                                    height: 30,
                                }}>
                                Danh sách chương trình
                            </Text>
                            <View
                                style={[shadow,
                                    {
                                        backgroundColor: '#fff',
                                        marginBottom: width * 0.02,
                                        marginHorizontal: width * 0.15,
                                        borderRadius: 5,
                                        paddingHorizontal: width * 0.04,
                                        paddingVertical: WidthPercent * 5,
                                    },
                                ]}
                            >
                                <TouchableOpacity
                                    style={{
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        borderRadius: width * 0.07,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        paddingHorizontal: width * 0.05,
                                        alignItems: 'center',
                                        height: WidthPercent * 10,
                                        backgroundColor: '#fff',
                                    }}
                                    onPress={_openListCategory}
                                >
                                    <Text style={{color: '#333333', flex: 1, paddingRight: 15}}
                                          numberOfLines={titleCategory !== 'Chọn danh mục' ? 1 : null}>{titleCategory}</Text>
                                    <Image
                                        resizeMode="contain"
                                        style={{
                                            width: width * 0.05,
                                            height: width * 0.05,
                                            transform: [{rotate: showCategory ? '180deg' : '0deg'}],
                                        }}
                                        source={require('../../Image/Icons/iconDown.png')}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        borderRadius: width * 0.07,
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        height: WidthPercent * 10,
                                        alignItems: 'center',
                                        paddingHorizontal: width * 0.05,
                                        marginTop: WidthPercent * 3,
                                        backgroundColor: '#fff',
                                    }}
                                    onPress={_openListProgramme}
                                    onLongPress={_openListProgramme}
                                >
                                    <Text style={{color: '#333333', flex: 1, paddingRight: 15}}
                                          numberOfLines={titleCategory !== 'Chọn chương trình' ? 1 : null}>{titleCurriculum}</Text>
                                    <Image
                                        resizeMode="contain"
                                        style={{
                                            width: width * 0.05,
                                            height: width * 0.05,
                                            transform: [{rotate: showProgramme ? '180deg' : '0deg'}],
                                        }}
                                        source={require('../../Image/Icons/iconDown.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <Text
                                style={{
                                    fontWeight: '700',
                                    fontSize: 16,
                                    color: '#0E0D42',
                                    marginLeft: width * 0.09,
                                }}>
                                {numberCourse} khóa học
                            </Text>
                            {
                                itemCourse.map(item => {
                                    return (
                                        <ItemCourse
                                            item={item}
                                            showCategory={showCategory}
                                            showProgramme={showProgramme}
                                            navigation={() => _toCourseInformation(item.CourseID)}
                                        />
                                    );
                                })
                            }
                        </View>
                    ) : (
                        <View
                            style={{
                                marginHorizontal: width * 0.06,
                                marginVertical: height * 0.02,
                            }}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <Image
                                    source={require('../../Image/Icons/iconHotline.png')}
                                    style={{width: width * 0.1, height: width * 0.1}}
                                    resizeMode={'contain'}
                                />
                                <Text
                                    style={{
                                        fontWeight: '700',
                                        color: '#0e0d42',
                                        fontSize: width * 0.05,
                                        marginLeft: width * 0.01,
                                    }}>
                                    Hotline
                                </Text>
                            </View>

                            <View
                                style={[
                                    shadow,
                                    {
                                        backgroundColor: '#fff',
                                        borderRadius: 5,
                                        paddingBottom: height * 0.03,
                                        paddingHorizontal: width * 0.03,
                                        marginTop: height * 0.01,
                                    },
                                ]}>
                                <FlatList
                                    data={itemAddressHotLine}
                                    renderItem={ItemHotLine}
                                    keyExtractor={(item, index) => {
                                        return item.toString() + index.toString();
                                    }}
                                />
                            </View>
                        </View>
                    )}
                </ScrollView>
            </View>
            <TouchableWithoutFeedback onPress={() => dispatch(showHotline(!show_hotline))}>
                <Image
                    source={require('../../Image/image/imageContact.png')}
                    style={{
                        position: 'absolute',
                        bottom: height * 0.02,
                        right: width * 0.03,
                        width: width * 0.17,
                        height: width * 0.17,
                        zIndex: 1,
                    }}
                    resizeMode={'contain'}
                />
            </TouchableWithoutFeedback>
        </View>
);
};