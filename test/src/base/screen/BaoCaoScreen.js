import React from 'react';
import {
    Text, ImageBackground, StatusBar, View, Image, Dimensions, StyleSheet
} from 'react-native';
import { Picker } from '@react-native-community/picker'
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import ProgressCircle from 'react-native-progress-circle';
import LinearGradient from "react-native-linear-gradient";

var Width = Dimensions.get("window").width;
var Height = Dimensions.get('window').height;
StatusBar.setHidden(false, 'slide');
export default function BaoCaoScreen({ route, navigation }) {
    const [search, updateSearch] = React.useState('');
    const [selectedValue, setSelectedValue] = React.useState('Chọn Danh mục');
    const [selectedCT, setCT] = React.useState('Chọn chương trình');
    return (
        <View >
            <ImageBackground
                style={{ width: Width, height: Height, resizeMode: 'contain' }}
                source={require('../../../image/nen1.png')}
            >
                <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 20
                }}>
                    <View
                        style={{
                            flexDirection: "row",
                            marginTop: 5                     
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                navigation.openDrawer();
                            }}
                        >
                            {/* <View style={{ width: 100, height: 30, marginLeft: 50, marginRight: -80, marginTop: 20, backgroundColor: 'white' }}> */}
                            <Image source={require('../../../image/setting.png')}
                                style={{ width: 40, height: 30, marginLeft: 70, marginRight: 0, marginTop: 20 }}
                            />
                            {/* </View> */}
                        </TouchableOpacity >
                        <View style={{
                            backgroundColor: 'white',
                            borderRadius: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: .5,
                            borderColor: '#000',
                            height: 50,
                            borderRadius: 15,
                            marginTop: 15,
                            marginLeft: 20,
                            marginRight: 50,
                            marginBottom: 0,
                        }}>
                            <TextInput style={{ fontSize: 18, color: 'black', width: 250 }} placeholder='Tìm kiếm khóa học' placeholderTextColor='gray'></TextInput>
                            <Image source={require('../../../image/search.png')} style={{ marginRight: 10, width: 20, height: 20 }} />
                        </View>
                    </View>
                    <View style={{ flex: 1, marginTop: 40 }}>
                        <ScrollView style={styles.scrollView, { width: Width, marginLeft: 0, marginBottom: 120 }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ fontWeight: 'bold', marginTop: 5, marginLeft: 30 }}>Trạng thái khóa học</Text>
                                <View style={{ backgroundColor: '#fff', width: 230, height: 80, alignItems: 'center', marginTop: 10, borderRadius: 10, marginLeft: 85, marginBottom: 15 }}>
                                    <View style={{ flexDirection: 'column' }}>
                                        <View style={{ marginTop: 25, justifyContent: 'center', alignItems: 'center', borderRadius: 40, borderWidth: 1, borderColor: '#04c2e5', width: 180, height: 30, marginBottom: 20 }}>
                                            <Picker
                                                selectedValue={selectedValue}
                                                style={{ height: 50, width: 150, color: 'green' }}
                                                mode={"dropdown"}
                                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)
                                                }
                                            >
                                                <Picker.Item label="Trạng thái 1" value="Trạng thái 1" />
                                                <Picker.Item label="Trạng thái 2" value="Trạng thái 2" />
                                                <Picker.Item label="Trạng thái 3" value="Trạng thái 3" />
                                                <Picker.Item label="Trạng thái 4" value="Trạng thái 4" />
                                                <Picker.Item label="Trạng thái 5" value="Trạng thái 5" />
                                            </Picker>
                                        </View>
                                    </View>
                                </View>
                                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#B6EAF5', '#C5C2EA']} style={{ width: 375, paddingLeft: 0, borderRadius: 30, marginLeft: 10 }} >
                                    <View style={{ marginTop: 10, width: 365, borderRadius: 30, flex: 1, flexDirection: 'column', marginLeft: 15, marginRight: 15 }}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 10, marginLeft: 25, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 85 }}>
                                            <Image source={require('../../../image/star.png')} style={{ marginLeft: -35, width: 20, height: 20 }} />
                                            <Text style={{ marginLeft: 10, width: 220, fontWeight: 'normal', fontSize: 16, marginTop: 5 }}>Các sản phẩm và dịch vụ 3G (Scorm710)</Text>
                                            <View style={{ marginLeft: 40 }}>
                                                <ProgressCircle
                                                    percent={0}
                                                    radius={20}
                                                    borderWidth={6}
                                                    color="#3399FF"
                                                    shadowColor="#DCDCDC"
                                                    bgColor="#fff"
                                                >
                                                    <Text style={{ fontSize: 10 }}>{'0%'}</Text>
                                                </ProgressCircle>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 10, marginLeft: 25, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 85 }}>
                                            <Image source={require('../../../image/star50.png')} style={{ marginLeft: -35, width: 20, height: 20 }} />
                                            <Text style={{ marginLeft: 10, width: 220, fontWeight: 'normal', fontSize: 16, marginTop: 5 }}>Đặc điểm,quyền lợi,điều kiện tham gia VITA sống thịnh vượng(Scorm950)</Text>
                                            <View style={{ marginLeft: 40 }}>
                                                <ProgressCircle
                                                    percent={50}
                                                    radius={20}
                                                    borderWidth={6}
                                                    color="#3399FF"
                                                    shadowColor="#DCDCDC"
                                                    bgColor="#fff"
                                                >
                                                    <Text style={{ fontSize: 10 }}>{'50%'}</Text>
                                                </ProgressCircle>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 10, marginLeft: 25, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 85 }}>
                                            <Image source={require('../../../image/star100.png')} style={{ marginLeft: -35, width: 20, height: 20 }} />
                                            <Text style={{ marginLeft: 10, width: 220, fontWeight: 'normal', fontSize: 16, marginTop: 5 }}>Giới thiệu công ty quản lý quỹ và quỹ liên kết đơn vị(Scorm955)</Text>
                                            <View style={{ marginLeft: 40 }}>
                                                <ProgressCircle
                                                    percent={70}
                                                    radius={20}
                                                    borderWidth={6}
                                                    color="#3399FF"
                                                    shadowColor="#DCDCDC"
                                                    bgColor="#fff"
                                                >
                                                    <Text style={{ fontSize: 10 }}>{'70%'}</Text>
                                                </ProgressCircle>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 10, marginLeft: 25, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 85 }}>
                                            <Image source={require('../../../image/star50.png')} style={{ marginLeft: -35, width: 20, height: 20 }} />
                                            <Text style={{ marginLeft: 10, width: 220, fontWeight: 'normal', fontSize: 16, marginTop: 5 }}>Kỹ năng thuyết trình    ( corm826)</Text>
                                            <View style={{ marginLeft: 40 }}>
                                                <ProgressCircle
                                                    percent={25}
                                                    radius={20}
                                                    borderWidth={6}
                                                    color="#3399FF"
                                                    shadowColor="#DCDCDC"
                                                    bgColor="#fff"
                                                >
                                                    <Text style={{ fontSize: 10 }}>{'25%'}</Text>
                                                </ProgressCircle>
                                            </View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 10, marginLeft: 25, flex: 1, flexDirection: 'row', marginRight: 70, backgroundColor: 'white', borderRadius: 10, width: 320, height: 85 }}>
                                            <Image source={require('../../../image/star100.png')} style={{ marginLeft: -35, width: 20, height: 20 }} />
                                            <Text style={{ marginLeft: 10, width: 220, fontWeight: 'normal', fontSize: 16, marginTop: 5 }}>Oriflame - Module 4 - Các nội dung cơ bản của Hợp đồng tham gia bán hàng đa cấp (Scorm960)</Text>
                                            <View style={{ marginLeft: 40 }}>
                                                <ProgressCircle
                                                    percent={70}
                                                    radius={20}
                                                    borderWidth={6}
                                                    color="#3399FF"
                                                    shadowColor="#DCDCDC"
                                                    bgColor="#fff"
                                                >
                                                    <Text style={{ fontSize: 10 }}>{'70%'}</Text>
                                                </ProgressCircle>
                                            </View>
                                        </View>
                                    </View>
                                </LinearGradient>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00D0BF',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        marginTop: 30,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 0,
    },
    floatingButtonStyle: {
        position: 'absolute',
        bottom: 120,
        right: 0,
        //backgroundColor:'black'
    },
    scrollView: {
    },
});