import React, {useState, useEffect} from 'react';
import {
    Image,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    FlatList, ScrollView, RefreshControl, Alert,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const WidthPercent = width / 100;
const HeightPercent = height / 100;
import HeaderCategory from '../../components/HeaderCategory';
import ItemClass from '../../components/ItemClass';
import API from '../../api/APIConstant';
import axios from 'axios';
import LoadingScreen from '../../components/LoadingScreen';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';

const CategoryScreen = (props) => {
    const {userInfo} = useSelector(state => state.infoUser);
    const [scrollEnabled, setScrollEnable] = useState(true);
    const [refreshEnable, setRefreshEnable] = useState(true);
    const [refreshing, setRefreshing] = useState(false);
    const [itemCourse, setItemCourse] = useState([]);
    const [itemCategory, setItemCategory] = useState([]);
    const [itemProgramme, setItemProgramme] = useState([]);
    const [numberCourse, setNumberCourse] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [categoryId, setCategoryId] = useState('14');
    const [curriculumId, setCurriculumId] = useState('17');
    const [ModalCategory, setModalCategory] = useState(false);
    const [ModalCurriculum, setModalCurriculum] = useState(false);
    const [itemList, setItemList] = useState(false);
    const [Top, setTop] = useState(HeightPercent * 18 + 47 + 10 + HeightPercent * 18 * 0.15);
    const [keySearch, setKeySearch] = useState('');
    const [titleCategory, setTitleCategory] = useState('Chọn danh mục');
    const [titleCurriculum, setTitleCurriculum] = useState('Chọn chương trình');

    useEffect(() => {
        _getCategories();
    }, [userInfo]);

    useEffect(() => {
        _getCurriculums();
    }, [userInfo, categoryId]);

    // useEffect(() => {
    //     _getItemCourse('');
    // }, [userInfo]);

    const _onRefresh = () => {
        _getCategories();
        _getCurriculums();
        if (keySearch) {
            _searchCourse();
        } else {
            _filterCourse();
        }
        setRefreshing(false);
    };

    const _getCategories = async () => {
        const url = API.baseurl + API.categories + API.og_code;
        try {
            const res = await axios({method: 'get', url: url});
            if (res.data.status) {
                let data = {};
                data['CategoryID'] = 'all';
                data['CategoryName'] = 'Tất cả';
                let dataCategory = res.data.data_category;
                dataCategory.unshift(data);
                setItemCategory(dataCategory);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const _getCurriculums = async () => {
        const url = API.baseurl + API.curriculums + API.og_code + '&' + API.categoryID(categoryId);
        try {
            const res = await axios({method: 'get', url: url});
            if (res.data.status) {
                let data = {};
                data['CurriculumID'] = 'all';
                data['CurriculumName'] = 'Tất cả';
                let dataCurriculum = res.data.data_curriculum;
                dataCurriculum.unshift(data);
                setItemProgramme(dataCurriculum);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const _getItemCourse = async (item) => {
        setScrollEnable(true);
        setRefreshEnable(true);
        if (item.CategoryID) {
            setModalCategory(false);
            let title = item.CategoryID === 'all' ? 'Chọn danh mục' : item.CategoryName;
            let categoryIdNew = item.CategoryID === 'all' ? '' : item.CategoryID;
             setCurriculumId('');
             setTitleCategory(title);
             setCategoryId(categoryIdNew);
             setTitleCurriculum('Chọn chương trình');
        }
        if (item.CurriculumID) {
            setModalCurriculum(false);
            let title = item.CurriculumID === 'all' ? 'Chọn chương trình' : item.CurriculumName;
            let curriculumIDNew = item.CurriculumID === 'all' ? '' : item.CurriculumID;
             setTitleCurriculum(title);
             setCurriculumId(curriculumIDNew);
        }
    };

    useEffect(() => {
        if (keySearch) {
            _searchCourse();
        } else {
            _filterCourse();
        }
    }, [categoryId, curriculumId]);

    const _filterCourse = async () => {
        setIsLoading(true);
        const url = API.baseurl + API.courses + 'CategoryID=' + categoryId + '&CurriculumID=' + curriculumId + API.og_code + '&' + 
        //API.userLogonID(userInfo.username)
        'UserLogonID=100005'
        ;
        try {
            const res = await axios({method: 'get', url: url});
            await setItemCourse(res.data.list_course);
            await setNumberCourse(res.data.list_course.length);
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            await setItemCourse([]);
            await setNumberCourse([]);
            Alert.alert('Thông báo', 'Không tải được danh sách khóa học.', [
                    {text: 'Đồng ý'},
                ],
                {cancelable: false});
            setIsLoading(false);
        } finally {
            setTimeout(() => {
                setIsLoading(false);
            }, 30000);
        }
    };

    const _goBack = () => {
        props.navigation.goBack();
    };

    const _searchCourse = async () => {
        setIsLoading(true);
        const url = API.baseurl + API.searchCourseCategory + 'UserLogonID=' + userInfo.username + '&search_value=' + keySearch + API.og_code + '&' + API.categoryID(categoryId) + '&' + API.curriculumID(curriculumId);
        try {
            const res = await axios({method: 'get', url: url});
            await setItemCourse(res.data.list_course);
            await setNumberCourse(res.data.list_course.length);
            setIsLoading(false);
        } catch (error) {
            console.log(error);
            await setItemCourse([]);
            await setNumberCourse(0);
            Alert.alert('Thông báo', 'Không tải được danh sách khóa học.', [
                    {text: 'Đồng ý'},
                ],
                {cancelable: false});
            setIsLoading(false);
        } finally {
            setTimeout(() => {
                setIsLoading(false);
            }, 30000);
        }
    };

    const _changeText = (text) => {
        setKeySearch(text);
    };

    const _openDrawer = () => {
        props.navigation.openDrawer();
    };

    const _openCategory = () => {
        if (ModalCurriculum){
            setModalCurriculum(false);
            setScrollEnable(true);
            setRefreshEnable(true);
        } else {
            setModalCategory(!ModalCategory);
            setScrollEnable(!scrollEnabled);
            setRefreshEnable(!refreshEnable);
        }
    };

    const _openCurriculum = () => {
        if (ModalCategory){
            setModalCategory(false);
            setScrollEnable(true);
            setRefreshEnable(true);
        } else {
            setModalCurriculum(!ModalCurriculum);
            setScrollEnable(!scrollEnabled);
            setRefreshEnable(!refreshEnable);
        }
    };

    return (
        <View style={{flex: 1, backgroundColor: '#f0f0f0', zIndex: 1}}>
                {HeaderCategory({
                    page: 'Category',
                    placeHolder: 'Tìm kiếm khóa học...',
                    _openDrawer,
                    _goBack,
                    _changeText,
                    _searchCourse,
                    keySearch: keySearch,
                })}
                <ScrollView contentContainerStyle={{flexGrow: 1}} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={_onRefresh} enabled={refreshEnable}/>} scrollEnabled={scrollEnabled}>
                    <View style={{flex: 1, marginTop: 10, paddingHorizontal: 20}}>
                        {
                            ModalCategory &&
                                    <View style={{
										position: 'absolute',
										top: 30 + 5 + WidthPercent * 5 + WidthPercent * 9.3 + 10,
										left: 20 + (width - 40) * 0.22,
										right: 20 + (width - 40) * 0.22,
                                        backgroundColor: '#fff',
                                        borderRadius: width * 0.05,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        maxHeight: height * 0.3,
                                        shadowColor: '#000',
                                        shadowOffset: {
                                            width: 0,
                                            height: 4,
                                        },
                                        shadowOpacity: 0.30,
                                        shadowRadius: 4.65,
										elevation: 8,
										zIndex: 4
                                    }}>
                                        <FlatList data={itemCategory}
                                                  renderItem={({item}) => (
                                                      <TouchableOpacity style={{paddingHorizontal: 15}}
                                                                        onPress={() => _getItemCourse(item)}>
                                                          <Text style={{
                                                              color: '#666666',
                                                              fontFamily: 'Myriad Pro',
                                                              fontWeight: '400',
                                                              marginVertical: 5,
                                                          }}>{item.CategoryName}</Text>
                                                      </TouchableOpacity>
                                                  )}
                                        />
                                    </View>
                        }
                        {
                            ModalCurriculum &&
                                    <View style={{
										position: 'absolute',
										top: 30 + 5 + WidthPercent * 5 + WidthPercent * 9.3 + WidthPercent * 3 + WidthPercent * 9.3 + 10,
										left: 20 + (width - 40) * 0.22,
										right: 20 + (width - 40) * 0.22,
                                        backgroundColor: '#fff',
                                        borderRadius: width * 0.05,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        maxHeight: height * 0.3,
                                        shadowColor: '#000',
                                        shadowOffset: {
                                            width: 0,
                                            height: 4,
                                        },
                                        shadowOpacity: 0.30,
                                        shadowRadius: 4.65,
										elevation: 8,
										zIndex: 4
                                    }}>
                                        <FlatList data={itemProgramme}
                                                  renderItem={({item}) => (
                                                      <TouchableOpacity style={{paddingHorizontal: 15}}
                                                                        onPress={() => _getItemCourse(item)}>
                                                          <Text style={{
                                                              color: '#666666',
                                                              fontFamily: 'Myriad Pro',
                                                              fontWeight: '400',
                                                              marginVertical: 5,
                                                          }}>{item.CurriculumName}</Text>
                                                      </TouchableOpacity>
                                                  )}
                                        />
                                    </View>
                        }
                        <Text style={{
                            color: '#0e0d42',
                            fontFamily: 'Myriad Pro',
                            fontSize: 16,
                            fontWeight: '700',
                            height: 30,
                        }}>Danh sách chương trình</Text>
                        <View style={{
                            width: '100%',
                            alignItems: 'center',
                            marginTop: 5,
                        }}>
                            <View style={{
                                width: '70%',
                                // height: HeightPercent * 10,
                                backgroundColor: '#FFFFFF',
                                alignItems: 'center',
                                paddingVertical: WidthPercent * 5,
                                borderRadius: 10,
                                shadowColor: '#000',
                                shadowOffset: {
                                    width: 0,
                                    height: 4,
                                },
                                shadowOpacity: 0.30,
                                shadowRadius: 4.65,
                                elevation: 8,
                            }}>
                                <View style={{
                                    width: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                }}>
                                    <TouchableOpacity style={{
                                        width: '80%',
                                        height: WidthPercent * 9.3,
                                        backgroundColor: '#FFFFFF',
                                        borderRadius: 20,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        paddingHorizontal: 15,
                                        justifyContent: 'space-between'
                                    }}
                                                      onPress={_openCategory}
                                    >
                                        <Text
                                            style={{
                                                color: '#333333',
                                                fontFamily: 'Myriad Pro',
                                                fontWeight: '400',
                                                flex: 1,
                                                paddingRight: 15,
												fontSize: width * 0.035,
												marginTop: Platform.OS === 'ios' ? 5 : 0
                                            }}
                                            numberOfLines={titleCategory !== 'Chọn danh mục' ? 1 : null}
                                        >{titleCategory}</Text>
                                        <Image style={{
                                            width: 20,
                                            height: 15,
                                            transform: [{rotate: ModalCategory ? '180deg' : '0deg'}]
                                        }}
                                               source={require('../../Image/Icons/Arow.png')}
                                               resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={{
                                    width: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    marginTop: WidthPercent * 3
                                }}>
                                    <TouchableOpacity style={{
                                        width: '80%',
                                        height: WidthPercent * 9.3,
                                        backgroundColor: '#FFFFFF',
                                        borderRadius: 20,
                                        borderWidth: 1,
                                        borderColor: '#5adcff',
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        paddingHorizontal: 15,
                                        justifyContent: 'space-between'
                                    }}
                                                      onPress={_openCurriculum}
                                    >
                                        <Text
                                            style={{
                                                color: '#333333',
                                                fontFamily: 'Myriad Pro',
                                                fontWeight: '400',
                                                flex: 1,
                                                paddingRight: 15,
                                                fontSize: width * 0.035,
                                                marginTop: Platform.OS === 'ios' ? 5 : 0
                                            }}
                                            numberOfLines={titleCurriculum !== 'Chọn chương trình' ? 1 : null}
                                        >{titleCurriculum}</Text>
                                        <Image style={{
                                            width: 20,
                                            height: 15,
                                            transform: [{rotate: ModalCurriculum ? '180deg' : '0deg'}]
                                        }}
                                               source={require('../../Image/Icons/Arow.png')}
                                               resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <Text style={{
                            color: '#0e0d42',
                            fontFamily: 'Myriad Pro',
                            fontSize: 16,
                            fontWeight: '700',
                            marginTop: 20,
                        }}>{numberCourse} khóa học</Text>
                        <View style={{flex: 1}}>
                            {
                                itemCourse.map(item => {
                                        return (
                                            <ItemClass
                                                item={item}
                                                navigation={() => props.navigation.navigate('CourseInformation', {
                                                    register: true,
                                                    CourseId: item.CourseID,
                                                    _onRefresh,
                                                })}/>
                                        );
                                    },
                                )
                            }
                        </View>
                    </View>
                </ScrollView>
        </View>
    );
};
export default CategoryScreen;
