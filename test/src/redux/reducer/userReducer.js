import Types from '../types';
// Initial State
const initialState = {
  userInfo: {}
};

// Reducers (Modifies The State And Returns A New State)
const authLogin = (state = initialState, action) => {
  const { type, param } = action;
  switch (type) {
    // Change og code
    case Types.LOGIN_SUCCESS: {
      return {
        // State
        ...state,
        // Redux Store
        userInfo: param,
      }
    }

    // Default
    default: {
      return state;
    }
  }
};

// Exports
export default authLogin;