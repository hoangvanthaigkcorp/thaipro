import Types from '../types';

const initialState = {
    og_data: ''
  };

const OG_Data = (state = initialState, action) => {
    const { type, param } = action;
    switch (type) {
      // Change og code
      case Types.SET_OG_DATA: {
        return {
          // State
          ...state,
          // Redux Store
          og_data: param,
        }
      }
  
      // Default
      default: {
        return state;
      }
    }
};

export default OG_Data;
