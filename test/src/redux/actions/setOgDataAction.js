import Types from '../types';
const setOgData = (param) => ({
    type: Types.SET_OG_DATA,
    param
});

export {setOgData}