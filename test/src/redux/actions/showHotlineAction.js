import Types from '../types';

const showHotline = (param) => ({
    type: Types.SHOW_HOTLINE,
    param
});

export {showHotline}